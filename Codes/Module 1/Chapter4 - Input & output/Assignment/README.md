# **Basic Calculator**

> ### Operations performed by calculator are
> * Addition 
> * Subtraction
> * Multiplication
> * Division

#### **Error Handeling**
>  #### Error is possible at only one position   
> * During entering values in two input places i.e. if value entered is string or non numerical value i.e. symbol then give alert as string value not allowed or enter a valid number

#### **How to use it :**
> - Enter Two float (decimal) ,integer numbers in the box
> - Select the operation you want to perform   
> - Answer would be shown in the box as well as a dialog box will be shown with the answer. 

