> - Headings

 # This is header

> - Italic  

*This is italic*  
_This line is also italic_   
<i> this is italic</i>

> - Bold    
 
**This is bold**  
__This is bold__  
<b> this is bold</b>

> - Line breaks  

> - Blockquote

>This is a blockquote

> - Nested BlockQuote


>This is a blockquote
>>this is nested blockquote

> - Ordered List   

1. apple
2. mango
3. banana

> - Unordered List

* apple
+ banana
- mango

> - Adding Links

[Vlabs](http://vlabs.iitb.ac.in/vlab/ "Vlabs")

> - Email & URL's

<support@vlab.co.in>  
support@vlab.co.in
<br>
To disable link
'support@vlab.co.in'

> - Tables

|Name|Roll no|Class|
|----|----|----|
|Rajeev|064|FE|
|Rajeev|064|FE|


|Name|Roll no|Class|
|:----|----:|:----:|
|Rajeev|064|FE|
|Rajeev|064|FE|

> - Images

![Flower](./flower.jpg "Flower")

![Thank you](https://cdn.pixabay.com/photo/2016/11/24/14/00/christmas-tree-1856343__340.jpg "Car")
